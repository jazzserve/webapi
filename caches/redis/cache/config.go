package cache

import (
	"time"
)

const (
	network            = "tcp"
	defaultMaxIdle     = 2
	defaultIdleTimeOut = time.Minute * 5
)

type Config struct {
	Address     string        `yaml:"address" validate:"required"`
	DB          int           `yaml:"db" validate:"required"`
	MaxIdle     int           `yaml:"max-idle"`
	IdleTimeOut time.Duration `yaml:"idle-time-out"`
}
