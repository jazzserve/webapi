package cache

import (
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
)

var NilConfigErr = errors.New("can't create new redis cache: nil config")

type RedisCache struct {
	pool *redis.Pool
}

func NewCache(c *Config) (*RedisCache, error) {
	if c == nil {
		return nil, NilConfigErr
	}

	r := &RedisCache{
		pool: &redis.Pool{
			MaxIdle:     c.MaxIdle,
			IdleTimeout: c.IdleTimeOut,
			Dial: func() (redis.Conn, error) {
				conn, err := redis.Dial(network, c.Address, redis.DialDatabase(c.DB))
				if err != nil {
					return nil, errors.WithStack(err)
				}
				if _, err := conn.Do("SELECT", c.DB); err != nil {
					conn.Close()
					return nil, errors.WithStack(err)
				}
				return conn, nil
			},
		},
	}

	if c.IdleTimeOut == 0 {
		r.pool.IdleTimeout = defaultIdleTimeOut
	}

	if c.MaxIdle == 0 {
		r.pool.MaxIdle = defaultMaxIdle
	}

	return r, nil
}

func (r *RedisCache) Get() redis.Conn {
	return r.pool.Get()
}
