package payload

import (
	"bitbucket.org/jazzserve/webapi/utils/pointers/ptr"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"strings"
)

type HttpError struct {
	error
	status int
	code   *string
}

type ValidationErrorResponseBody struct {
	Code    *string                        `json:"code"`
	Message *string                        `json:"message,omitempty"`
	Errors  []*ValidationErrorResponseItem `json:"errors"`
}

type ValidationErrorResponseItem struct {
	Field *string `json:"field"`
	Tag   *string `json:"tag"`
}

func (e HttpError) Unwrap() error {
	return e.error
}

func (e HttpError) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Message string  `json:"message"`
		Code    *string `json:"code,omitempty"`
	}{
		Message: e.Error(),
		Code:    e.code,
	})
}

func getHttpError(err error) (httpErr *HttpError) {
	ok := errors.As(err, &httpErr)
	if !ok {
		return WrapInternalServerErr(err)
	}
	return httpErr
}

func ErrorResponse(w http.ResponseWriter, err error) {
	if w == nil {
		panic("ErrorResponse: nil ResponseWriter")
	}
	if err == nil {
		return
	}

	httpErr := getHttpError(err)
	w.WriteHeader(httpErr.status)
	JsonEncode(w, httpErr)
	if httpErr.status == http.StatusInternalServerError {
		log.Error().Msgf("Responded with 500: %+v", err)
	}
}

func ValidationErrorResponse(w http.ResponseWriter, err error) {
	if w == nil {
		panic("ValidationErrorResponse: nil ResponseWriter")
	}
	if err == nil {
		return
	}

	var validationErrs validator.ValidationErrors
	if !errors.As(err, &validationErrs) {
		ErrorResponse(w, err)
		return
	}

	res := ValidationErrorResponseBody{
		Code:    ptr.Str("VALIDATION"),
		Message: ptr.Str(err.Error()),
	}
	for _, err := range validationErrs {
		res.Errors = append(res.Errors, &ValidationErrorResponseItem{
			Field: ptr.Str(err.StructNamespace()),
			Tag:   ptr.Str(strings.ReplaceAll(strings.ToUpper(err.Tag()), "-", "_")),
		})
	}

	w.WriteHeader(http.StatusBadRequest)
	JsonEncode(w, res)
	log.Error().Msgf("Responded with 400(validation): %+v", err)
}

func newErrorf(status int, format string, args ...interface{}) *HttpError {
	return &HttpError{
		error:  errors.Errorf(format, args...),
		status: status,
	}
}

func wrapError(status int, err error) *HttpError {
	if err == nil {
		return nil
	}
	if httpErr, ok := err.(*HttpError); ok {
		httpErr.status = status
		return httpErr
	}
	return &HttpError{
		error:  err,
		status: status,
	}
}

func IsHttpErr(err error) bool {
	_, ok := err.(*HttpError)
	return ok
}

func NewInternalServerErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusInternalServerError, format, args...)
}

func WrapInternalServerErr(err error) *HttpError {
	return wrapError(http.StatusInternalServerError, err)
}

func NewBadRequestErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusBadRequest, format, args...)
}

func NewPaymentRequiredErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusPaymentRequired, format, args...)
}

func WrapBadRequestErr(err error) *HttpError {
	return wrapError(http.StatusBadRequest, err)
}

func NewUnauthorizedErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusUnauthorized, format, args...)
}

func WrapUnauthorizedErr(err error) *HttpError {
	return wrapError(http.StatusUnauthorized, err)
}

func NewForbiddenErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusForbidden, format, args...)
}

func WrapForbiddenErr(err error) *HttpError {
	return wrapError(http.StatusForbidden, err)
}

func NewNotFoundErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusNotFound, format, args...)
}

func WrapNotFoundErr(err error) *HttpError {
	return wrapError(http.StatusNotFound, err)
}

func NewUnprocessableEntityErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusUnprocessableEntity, format, args...)
}

func WrapUnprocessableEntityErr(err error) *HttpError {
	return wrapError(http.StatusUnprocessableEntity, err)
}

func NewConflictErr(format string, args ...interface{}) *HttpError {
	return newErrorf(http.StatusConflict, format, args...)
}

func WrapConflictErr(err error) *HttpError {
	return wrapError(http.StatusConflict, err)
}

func WrapErrCode(err error, code string) *HttpError {
	httpErr := getHttpError(err)
	httpErr.code = &code
	return httpErr
}
