package store

import (
	"net/http"
	"time"
)

const (
	network            = "tcp"
	defaultCookiePath  = "/"
	defaultMaxIdle     = 1
	defaultIdleTimeOut = 5
	defaultCookieSecure   = true
	defaultCookieSameSite = http.SameSiteLaxMode
)

var cookieSameSite = map[string]http.SameSite{
	"lax":    http.SameSiteLaxMode,
	"strict": http.SameSiteStrictMode,
	"none":   http.SameSiteNoneMode,
}

type Config struct {
	Address string         `yaml:"address" validate:"required"`
	Key     string         `yaml:"key" validate:"required"`
	MaxAge  *time.Duration `yaml:"max-age" validate:"required"`
	DB      int            `yaml:"db" validate:"required"`
	Options struct {
		Path     *string `yaml:"path"`
		Secure   *bool   `yaml:"secure"`
		SameSite *string `yaml:"same-site"`
	} `yaml:"options"`
}
