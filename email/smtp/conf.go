package smtp

type Config struct {
	UserName   string `json:"username,omitempty" yaml:"user-name"`
	Password   string `json:"password,omitempty" yaml:"password"`
	Host       string `json:"host,omitempty" yaml:"host" validate:"required"`
	Port       int    `json:"port,omitempty" yaml:"port"`
	From       string `json:"from,omitempty" yaml:"from" validate:"required"`
	ReplyTo    string `json:"reply_to,omitempty" yaml:"reply-to"`
	SenderName string `json:"sender_name,omitempty" yaml:"sender-name"`
	TLS        bool   `json:"tls,omitempty" yaml:"tls"`
	SSL        bool   `json:"ssl,omitempty" yaml:"ssl"`
	AuthType   string `json:"auth_type,omitempty" yaml:"auth-type"`
}
