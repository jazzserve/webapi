package smtp

import (
	"errors"
	"net/smtp"
)

type tlsAuth struct {
	username, password string
}

func TLSAuth(username, password string) smtp.Auth {
	return &tlsAuth{username, password}
}

func (a *tlsAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *tlsAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unknown fromServer")
		}
	}
	return nil, nil
}
